import React, { Component } from 'react';
import logo from './logo.svg';
import './App.scss';

import Account from './containers/account';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
           Success!!
          </p>
          <a 
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer" 
          > 
            Learn React
          </a>
        </header>
        <Account />
      </div>
    );
  }
}

export default App;
