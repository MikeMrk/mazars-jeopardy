<?php 
require(APPPATH.'/libraries/REST_Controller.php');

class REST extends REST_Controller
{

	public function __construct()
	{

		parent::__construct();
        $this->load->model('general_model');
        
        $this->load->library('ion_auth');

        $this->logged_in =  $this->ion_auth->logged_in();
        $this->is_admin = $this->ion_auth->is_admin();
		  
    }

    public function index_get()
    {

        $this->response('hi');

    }
    
    public function login_post()
    {
        
		$email = $this->input->post('email');
        $password = $this->input->post('password');
        
        //$this->response($this->input->post());

        $response = [
            'success' => false
        ];

        if(!isset($email) || !isset($password))
		{
			$this->response(array('message' => 'Both email and password are required',
				'success' => false,
			), 200);
        }
        
        $success =  $this->ion_auth->login($email, $password, true);

       if($success){
           $response['success'] = true;
           $this->response($response);
       }


    }

    public function login_get()
    {
        $response = [];

        $response['logged_in'] = $logged_in = $this->ion_auth->logged_in();

        if($logged_in){
            $response['user_id'] = intval($user_id = $this->ion_auth->get_user_id());
            $response['is_admin'] = $this->ion_auth->is_admin();
        }
        $this->response($response);

    }

    public function logout_post()
    {

        $logout = $this->ion_auth->logout();

        $this->response(['success' => $logout]);


    }

    public function register_post()
    {

        if(!$this->logged_in){
            $response = ['message' => 'You are not authentificated'];
            $this->response($response);
        }

        if(!$this->is_admin){
            $response = ['message' => 'You are not admin'];
            $this->response($response);
        }

        

        $name = $this->input->post('name');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $email = $this->input->post('email');


        $response = [
            'success' => true,
        ];

        $this->load->model('account_model');
        $email_exists = $this->account_model->email_exists($email);

        if(!$username || !$password || !$email){
            $this->response(array(
                'message' => 'Required fields: username, password, email',
                'success' => false,
            ), 400);
        }  

        if($email_exists){
            $this->response(array(
                'message' => "Email address is already in use",
                'success' => false,
            ), 400);
        }

        $response['register'] = $register = $this->ion_auth->register($username, $password, $email);

        $this->response($response);

    }
 
}