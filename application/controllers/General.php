<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General extends CI_Controller {

    public function __construct()
    {
        
        parent::__construct();

        $this->load->library(['ion_auth', 'form_validation']);
		$this->load->helper(['url', 'language']);

    }

    public function index()
    {

        $this->load->model("general_model");
       
        $logged = $this->ion_auth->logged_in();
        //var_dump($logged);


    }

    public function hi()
    {

        echo 'hi';

    }

    public function react()
    {

        $this->load->view('react.html');

    }

}

