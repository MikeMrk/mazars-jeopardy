<?php

class general_model extends CI_Model
{

	
	 /***************
    /   Grabs some items from the database. It requires a table to be passed. If an ID is specificed, it grabs that single item that matches the ID. 

    $data represents the GET variables. It is an array that should have keys matching the columns from the database.

	IF $count is true, than it counts the total number of items matching the variables and return the value.


    ***************/
	public function read_general($table, $id = NULL, $data = NULL, $count = false, $search = NULL, $order_by = NULL, $join = NULL, $extra_query_size = NULL){

		$offset = "";

		// if no id is specified, get the list of all the products. 

		//determinte the list's dimension. maximum of 100 allowed.
		if(isset($data['request_size'][0])){
			$limit_val = $data['request_size'];
			if(is_array($data['request_size'])){
				$limit_val = $data['request_size'][0];
			}
			if($limit_val > 100)
			{
				$limit_val = 100;
			}
			unset($data['request_size']);
		} else {
			$limit_val = 100;	
		}

		if(isset($extra_query_size)){
			$limit_val = $extra_query_size;
		} 

		$limit_string = "LIMIT " . strval($limit_val);

		if(isset($data['page'])){
			if (isset($data['page'][0])){
					$offset = $limit_val * ($data['page'][0] - 1);
					
			}
			unset($data['page']);
		}


		// if(isset($data['orderby'])){
		// 	$order_by = array($data['orderby'][0], 'asc');
		// 	unset($data['orderby']);
		// }


		if(empty($data)){
			$getwhere = array();
		} else {
			$getwhere = '';

			//put all the GET data ($data) into a string, used later in the query for filtering
			foreach($data as $key => $param){
				if ($getwhere == '') {
					$getwhere = $getwhere . " " . $key . " in (";
					if(!is_array($param)){
						return 'Bad request. Parameters should be presented as array.';
					}
					foreach($param as $key => $p){
						if(is_string($p)){
							$p = "'" . $p . "'";
						}
						if($key == 0){
							$getwhere = $getwhere . $p;
						} else {
							$getwhere = $getwhere . "," . $p;
						}
					}
					$getwhere = $getwhere . ") "; 
				} else {
					$getwhere = $getwhere . "AND " . $key . " in (";
					foreach($param as $key => $p){
						if(is_string($p)){
							$p = "'" . $p . "'";
						}
						if($key == 0){
							$getwhere = $getwhere . $p;
						} else {
							$getwhere = $getwhere . "," . $p;
						}
					}
					$getwhere = $getwhere . ") "; 
				}
			}

		} 

		// return only the product matching the ID
		if($id){

			$query = $this->db->get_where($table, array('id' => $id));
			$result = $query->result_array();

			return $result;
		}

		if(isset($order_by)){
			$this->db->order_by($order_by[0], $order_by[1]);
		}

		if($count){
			$select= array("count(*) as Total");
			$this->db->select($select);
			$this->db->from($table);
			$this->db->where($getwhere);
			if(isset($search)){
				$this->pass_filters($search);
			}	
			$query = $this->db->get();
			return $query->row_object();

		} else {
			//$query = $this->db->get_where($table, $getwhere, $limit_val, $offset);


			$limit_str = $limit_val . ',' . $offset;

			$this->db->from($table);
			$this->db->where($getwhere);
			if(isset($order_by[0])){
				$this->db->order_by($order_by[0], $order_by[1]);
			}
			$this->db->limit($limit_val, $offset);
			if(isset($search)){
				//return $search;
				$this->pass_filters($search);
			}
			if(isset($join) && is_array($join)){
				$this->db->join($join[0], $join[1], $join[2]);
			}

			$query = $this->db->get();
			$result = $query->result_array();
		}

		return $result;

	}

	private function pass_filters($search){

		foreach($search as $key => $item)
		{
			$this->db->like($item->field, $item->data);
		}

	}

	 /***************
    /  Return a list of random items from the specified table (required). 

    If $size is specified, than the number of items returned will this variable's value. $size should have a 'safety' conditional that does not let it return more than a certain number of values (in order to not overload the server).

    To Do: Make it accept attributes. (should match the columns from the DB)
    ***************/
	public function read_random($table, $size = NULL, $sales_code = NULL){

		$max_request_size = 10;
		$limit_string = "LIMIT 10" . strval($max_request_size);

		if($size){
			if($size > $max_request_size){
				$size = $max_request_size;
			}
			$limit_string = "LIMIT " .strval($size); 
		}

		$query = $this->db->query("select * from `".$table."` where availability_code = 1 ORDER BY RAND() " . $limit_string);

		if($sales_code){
			$query = $this->db->query("select * from `".$table."` where availability_code = 1 and sales_code = '".$sales_code."' ORDER BY RAND() " . $limit_string);
		}

		$result = $query->result_array();

		return $result;

	}

	public function get_table_columns($table)
	{

		$query = $this->db->query("show columns from " . $table);
		$result = $query->result_array();

		$data = array();

		$not_allowed = array('id');

		foreach($result as $row)
		{
			
			if(!in_array($row['Field'], $not_allowed)){
				array_push($data, $row['Field']);
			}
			
		}

		return $data;
		
	}

	public function get_property_values($table, $properties)
	{

		$data = array();

		foreach($properties as $key => $property){

			$query = $this->db->query("SELECT DISTINCT " .$property. " from ". $table);
			$result = $query->result_array();

			$data[$property] = array();

			foreach($result as $row){
				array_push($data[$property], $row[$property]);
			}

		}
		return $data;

	}

	public function write_general($table, $id = NULL, $params)
	{

		$q = $this->db->insert_batch($table, $params);

		return $q;

	}

	public function write_single($table, $id, $params)
	{

		$this->db->insert($table, $params);

		$q = $this->db->insert_id();

		return $q;

	}

	public function update_batch($table, $data, $key)
	{

		$this->db = $this->load->database('default', TRUE);

		$this->update_batch($table, $data, $key);

		return 'ok';

	}

	public function update_general($table, $id, $params)
	{

		$this->db->where('id', $id);
		$q =$this->db->update($table, $params);


		return 1;

	}

	public function write_or_update($table, $data, $first_key = 0)
	{



		$keys = array_keys($data[$first_key]);


		$keys_string = "";

		$last_element = end($keys);

		$x = array_keys($data);

		foreach($keys as $key){
			$keys_string = $keys_string . '' . $key . '';
			if(!($key == $last_element)){
				$keys_string = $keys_string . ',';
			}
		}

		$val_str = '';


		//var_dump($last_element);

		foreach($data as $k => $element){

			$val_str = $val_str . '(';
			$last_param = end($element);


			//return $last_param;
			foreach($element as $tk => $param){
				$val_str = $val_str . "\"" . $param . "\" ";
				if(($tk !== $last_element)){
					//echo $param;
					$val_str = $val_str . ',';
				}
			}

			$val_str = $val_str . ')';
			if(($k != end($x))){
				$val_str = $val_str . ' , ';
			}
		}

		//var_dump($val_str);

		$update_val_str = '';

		foreach($keys as $key){
			$update_val_str = $update_val_str . '' . $key . '=VALUES('.$key.')';
			if(!($key == $last_element)){
				$update_val_str = $update_val_str . ',';
			}
		}


		// var_dump($val_str);
		// die();
		$query = 'INSERT INTO '.$table.' ( ' .$keys_string. ') values ' . $val_str . 'ON DUPLICATE KEY UPDATE ' .$update_val_str;

		//var_dump($query);

		//$query = mysqli_real_escape_string($query);


		return $this->db->query($query);
	}

	public function remove_keys($array, $keys)
	{

		$processed = $array;


		foreach($keys as $key)
		{


			if(isset($processed[$key])){
				unset($processed[$key]);
			}

		}

		
		return $processed;

	}


}