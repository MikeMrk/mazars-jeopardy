// import gulp from 'gulp'
// import run from 'gulp-run-command' 

var gulp = require('gulp');
var sass = require('gulp-sass');
var gulpCopy = require('gulp-copy');
var rename = require("gulp-rename");

var shell = require('gulp-shell');


gulp.task('copy_assets', function(){

    var sourceFiles = ['./build/**/*', ];
    return gulp
            .src(sourceFiles)
            .pipe(gulp.dest('./assets/'))

});

gulp.task('copy_html', function(){

    return gulp
            .src('./build/index.html')
            .pipe(rename('react.html'))
            .pipe(gulp.dest('./application/views/'))

})

gulp.task('build', shell.task(
    [
       'npm run-script build',
        'gulp copy_assets',
        'gulp copy_html'
    ]
))